Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kup
Upstream-Contact: Simon Persson <simonpersson1@gmail.com>
Source: https://www.linux-apps.com/p/1127689/

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in the file `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in the file `/usr/share/common-licenses/LGPL-2'.

Files: *
Copyright: 2011-2016 Simon Persson
License: GPL-2+

Files: debian/*
Copyright: 2017 Thomas Pierson <contact@thomaspierson.fr>
License: GPL-2+

Files: kcm/folderselectionmodel.cpp
       kcm/folderselectionmodel.h
Copyright: 2003 Scott Wheeler <wheeler@kde.org>
  2004 Mark Kretschmann <markey@web.de>
  2004 Max Howell <max.howell@methylblue.com>
  2008 Seb Ruiz <ruiz@kde.org>
  2008-2009 Sebastian Trueg <trueg@kde.org>
  2012 Simon Persson <simonpersson1@gmail.com>
License: LGPL-2

Files: kcm/kbuttongroup.cpp
       kcm/kbuttongroup.h
Copyright: 2006 Pino Toscano <toscano.pino@tiscali.it>
License: LGPL-2+
